@isTest
public class DatesChartTest {

    @isTest static void testYearRangeOne(){
        Date o = date.newInstance(2017, 8, 1);
        Date t = date.newInstance(2017, 9, 2);
        Integer yearR = DatesChartController.yearRange(o, t);
        System.assertEquals(1, yearR);
    }
    
    @isTest static void testYearRangeFour(){
        Date o = date.newInstance(2017, 8, 1);
        Date t = date.newInstance(2020, 9, 2);
        Integer yearR = DatesChartController.yearRange(o, t);
        System.assertEquals(4, yearR);
    }
    
    @isTest static void testConstHigh(){
        Date o = date.newInstance(2017, 8, 1);
        Date t = date.newInstance(2017, 9, 2);
        Date n = date.newInstance(2018, 1, 16);
        List<Date> listD = new List<Date>{o,t,n};
        Date hD = DatesChartController.ganttConstHigh(listD);
        System.assertEquals(n, hD);
    }
    
    @isTest static void testConstLow(){
        Date o = date.newInstance(2017, 8, 1);
        Date t = date.newInstance(2017, 9, 2);
        Date n = date.newInstance(2018, 1, 16);
        List<Date> listD = new List<Date>{o,t,n};
        Date lD = DatesChartController.ganttConstLow(listD);
        System.assertEquals(o, lD);
    }
    
    @isTest static void testvalsForGanttOneDate(){
        Date o = date.newInstance(2017, 8, 1);
        List<Date> listD = new List<Date>{o};
        DatesChartController.wrappOfValues vals = new DatesChartController.wrappOfValues();
        vals = DatesChartController.valuesForGantt(listD);
        System.assertEquals(vals.yStartVal,2017);
        System.assertEquals(vals.mStartVal,8);
        System.assertEquals(vals.dStartVal,1);
        System.assertEquals(vals.yRangeVal,1);
        System.assertEquals(vals.mRangeVal,5);
        System.debug('Count of Days OneDate' + vals.dRangeVal);
    }
    
    @isTest static void testvalsForGanttOneDateYCrossing(){
        Date o = date.newInstance(2017, 10, 1);
        List<Date> listD = new List<Date>{o};
        DatesChartController.wrappOfValues vals = new DatesChartController.wrappOfValues();
        vals = DatesChartController.valuesForGantt(listD);
        System.assertEquals(vals.yStartVal,2017);
        System.assertEquals(vals.mStartVal,10);
        System.assertEquals(vals.dStartVal,1);
        System.assertEquals(vals.yRangeVal,2);
        System.assertEquals(vals.mRangeVal,5);
        System.debug('Count of Days One Date YCrossing' + vals.dRangeVal);
    }
    
    @isTest static void testvalsForGanttTwoDsSameYNotCrossing(){
        Date o = date.newInstance(2017, 8, 1);
        Date t = date.newInstance(2017, 9, 2);
        List<Date> listD = new List<Date>{o,t};
        DatesChartController.wrappOfValues vals = new DatesChartController.wrappOfValues();
        vals = DatesChartController.valuesForGantt(listD);
        System.assertEquals(vals.yStartVal,2017);
        System.assertEquals(vals.mStartVal,8);
        System.assertEquals(vals.dStartVal,1);
        System.assertEquals(vals.yRangeVal,1);
        System.assertEquals(vals.mRangeVal,5);
        System.debug('Count of Days Two Dates NoCrossing' + vals.dRangeVal);
    }
    
    @isTest static void testvalsForGanttTwoDsSameYCrossing(){
        Date o = date.newInstance(2017, 10, 1);
        Date t = date.newInstance(2017, 11, 2);
        List<Date> listD = new List<Date>{o,t};
        DatesChartController.wrappOfValues vals = new DatesChartController.wrappOfValues();
        vals = DatesChartController.valuesForGantt(listD);
        System.assertEquals(vals.yStartVal,2017);
        System.assertEquals(vals.mStartVal,10);
        System.assertEquals(vals.dStartVal,1);
        System.assertEquals(vals.yRangeVal,2);
        System.assertEquals(vals.mRangeVal,5);
        System.debug('Count of Days 2 Dates YSame YCrossing' + vals.dRangeVal);
    }
    
    @isTest static void testvalsForGanttTwoDsDiffY(){
        Date o = date.newInstance(2017, 10, 1);
        Date t = date.newInstance(2018, 1, 2);
        List<Date> listD = new List<Date>{o,t};
        DatesChartController.wrappOfValues vals = new DatesChartController.wrappOfValues();
        vals = DatesChartController.valuesForGantt(listD);
        System.assertEquals(vals.yStartVal,2017);
        System.assertEquals(vals.mStartVal,10);
        System.assertEquals(vals.dStartVal,1);
        System.assertEquals(vals.yRangeVal,2);
        System.assertEquals(vals.mRangeVal,5);
        System.debug('Count of Days 2 Dates DiffY YCrossing' + vals.dRangeVal);
    }
    
    @isTest static void testvalsForGanttTwoDsWideRange(){
        Date o = date.newInstance(2017, 10, 1);
        Date t = date.newInstance(2018, 10, 5);
        List<Date> listD = new List<Date>{o,t};
        DatesChartController.wrappOfValues vals = new DatesChartController.wrappOfValues();
        vals = DatesChartController.valuesForGantt(listD);
        System.assertEquals(vals.yStartVal,2017);
        System.assertEquals(vals.mStartVal,10);
        System.assertEquals(vals.dStartVal,1);
        System.assertEquals(vals.yRangeVal,2);
        System.assertEquals(vals.mRangeVal,13);
    }
}