public class TasksUtils
{
    @InvocableMethod
    public static void AccountValidated(List<Id> accountIds)
    {
        List<Task> tasks=[select id from Task
                          where whatId in :accountIds
                          and Subject = 'Validate Account'];
 
        delete tasks;
    }
}