public class DatesChartController {
    private Date_Object__c dateObj;
    public static wrappOfValues timeVals{get;set;}
    
    //Gets the object records
    public DatesChartController(ApexPages.StandardController stdController){
        this.dateObj = (Date_Object__c)stdController.getRecord();
        //Checks if the date for insert is not null
        List<Date> ListofDs = new List<Date>();
        if(dateObj.Start_Date__c != null){
            ListofDs.add(dateObj.Start_Date__c);
        }
        if(dateObj.End_Date__c != null){
            ListofDs.add(dateObj.End_Date__c);
        }
        if(dateObj.Building_Date__c != null){
            ListofDs.add(dateObj.Building_Date__c);
        }
        wrappOfValues timeVals = valuesForGantt(ListofDs);
        system.debug(timeVals);
    }
    
    //Set of variables for dates values storage, visible for VF page
    public class wrappOfValues{
    	//Values storing starting positions for gantt
    	public Integer yStartVal;
    	public Integer mStartVal;
    	public Integer dStartVal;
    
    	//Values storing range values for gantt
    	public Integer yRangeVal;
    	public Integer mRangeVal;
    	public Integer dRangeVal;
    }
    
    //Method returning starting and range values for gantt proper display
    public static wrappOfValues valuesForGantt(List<Date> datesList){
        Integer crossYear;
        wrappOfValues wv = new wrappOfValues();
        
        //No need for getting constraints for gantt, list of dates contains only one date
        if(datesList.size() == 1){
            //Setting up what year,month and day to display
            wv.yStartVal = datesList[0].year();
            wv.mStartVal = datesList[0].month();
            wv.dStartVal = datesList[0].day();
            //Setting up range - 5 months display
            wv.mRangeVal = 5;
            //Check whether we are breaking trhough year
            crossYear = wv.mStartVal + 4;
            if(crossYear <= 12){
            	wv.yRangeVal = 1;
                wv.dRangeVal = datesList[0].daysBetween(date.newInstance(wv.yStartVal, wv.mStartVal + 4, wv.dStartVal)) + 1;
            }
            else{
                wv.yRangeVal = 2;
                //Setting of correct month for next year
                wv.dRangeVal = datesList[0].daysBetween(date.newInstance(wv.yStartVal + 1, 
                                                                         (wv.mStartVal + 4) - 12, wv.dStartVal)) + 1;
            }         
        }
        
        //Getting constraints for gantt, list of dates > 1
        else{
            Date leftConst = ganttConstLow(datesList);
        	Date rightConst = ganttConstHigh(datesList);
            
            //Setting up starting year,month and day
            wv.yStartVal = leftConst.year();
            wv.mStartVal = leftConst.month();
            wv.dStartVal = leftConst.day();
            
			//Getting months and years range between dates
			wv.yRangeVal = yearRange(leftConst, rightConst);
            wv.mRangeVal = leftConst.monthsBetween(rightConst);
            
            //Check whether we are breaking through year
            crossYear = wv.mStartVal + 4;
            
            //Setting up range for display - minimum of 5 months display
            if(wv.mRangeVal <= 4){
                wv.mRangeVal = 5;
                if(crossYear <= 12){
                    wv.dRangeVal = leftConst.daysBetween(date.newInstance(wv.yStartVal, wv.mStartVal + 4, wv.dStartVal)) + 1;
                }
                else{
                    wv.yRangeVal = 2;
                    //Setting of correct month for next year
                    wv.dRangeVal = leftConst.daysBetween(date.newInstance(wv.yStartVal + 1, 
																		 (wv.mStartVal + 4) - 12, wv.dStartVal)) + 1;
                } 
            }
            //Making sure, that all monhts that exceed 5 months range, will be displayed
            else{
                wv.mRangeVal = 1 + wv.mRangeVal;
                wv.dRangeVal = leftConst.daysBetween(rightConst) + 1; 
            }
            
        }
        
        return wv;
    }
    
    //Setting up of the left constraints for the gantt to be displayed
    public static Date ganttConstLow(List<Date> datesList){
    	Date lowestDate = date.newInstance(2500, 1, 1);        
        for(Integer i=0; i < datesList.size(); i++){
            if(lowestDate > datesList[i]){
                lowestDate = datesList[i];
            }
        }
        return lowestDate;
	}
    
    //Setting up of the right constraints for the gantt to be displayed
    public static Date ganttConstHigh(List<Date> datesList){
        Date highestDate = date.newInstance(1900, 1, 1);
        for(Integer i=0; i < datesList.size(); i++){
            if(highestDate < datesList[i]){
            	highestDate = datesList[i];
            }
        }
        return highestDate;
	}
    
    //Count of years to display in gantt
    public static Integer yearRange(Date lDate, Date hDate){
        Integer countOfYears = 1;
        if((hDate.year() - lDate.year()) == 0){
            return countOfYears;
        }
        else{
            for(Integer i = 0; i < (hDate.year() - lDate.year()); i++){
            	countOfYears = countOfYears + 1;
            }
            return countOfYears;
        }
    }

}