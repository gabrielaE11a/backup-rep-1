trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {
    List<Opportunity> oppList = new List<Opportunity>();
    List<Task> taskList = new List<Task>();
    
    for(Opportunity op : [SELECT Id, StageName FROM Opportunity WHERE Id IN :Trigger.New]){
        if(op.StageName == 'Closed Won'){
            Task t = new Task(Subject = 'Follow Up Test Task', WhatId = op.Id);
            taskList.add(t);
        }
    }
    insert taskList;
}